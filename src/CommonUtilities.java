import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

public class CommonUtilities {

	/**
	 * Returns a list of file names which is accessible from the given path.
	 * 
	 * @param storageLocation is the location where the files should be listed.
	 * @return list of file Strings or null if there was an error during the
	 *         process.
	 */
	static List<String> getFilesFromStorage(String storageLocation) {
		try {
			return Files.walk(Paths.get(storageLocation))
					.filter(Files::isRegularFile)
					.map(file -> file.getFileName().toString())
					.collect(Collectors.toList());

		} catch (IOException e) {
			System.err.println("An error occurred while listing the files");
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Returns true if the directory exists, false otherwise.
	 * 
	 * @param directoryName
	 * @return boolean, true if the directory exists, false otherwise.
	 */
	static boolean isDirectoryExists(String directoryName) {
		return new File(directoryName).exists();
	}

	/**
	 * Checks if the directory with the given name exists, creates it if not.
	 * 
	 * @param directoryName name of the directory that will be checked.
	 */
	static void checkForDirectory(String directoryName) {
		if (!isDirectoryExists(directoryName)) {
			new File(directoryName).mkdir();
		}
	}

	/**
	 * Receives a file from the sender, and it will be saved to the given
	 * directory(storageLocation).
	 * 
	 * 
	 * @param fileName        the name of the file that will be received.
	 * @param storageLocation the name of the directory where the file will be
	 *                        saved.
	 * @param in              the input stream where the file is coming from.
	 */
	static void receiveFile(String fileName, String storageLocation, ObjectInputStream in) {

		// Defining the bytes to read and write the file
		byte[] bytesToRead = new byte[1024];
		FileOutputStream fileStream;
		try {

			// Checks if the directory exists, if not then creates it
			checkForDirectory(storageLocation);
			fileStream = new FileOutputStream(
					Paths.get(storageLocation + "/" + fileName).toString());
			in.read(bytesToRead);
			fileStream.write(bytesToRead);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Sends a file to the receiver through the output stream.
	 * 
	 * @param fileName        the name of the file which will be sent.
	 * @param storageLocation is the name of the directory in which the file to be
	 *                        sent should be stored.
	 * @param out             output stream where the file will be sent though.
	 */
	static void sendFile(String fileName, String storageLocation, ObjectOutputStream out) {
		FileInputStream fileStream;
		try {
			fileStream = new FileInputStream(
					Paths.get(storageLocation + "/" + fileName).toString());
			byte b[] = new byte[1024];
			fileStream.read(b);
			out.write(b);
			out.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Sends a message to the receiver. Displays who sent the message.
	 * 
	 * @param message the message to be sent.
	 * @param sender  the name of the sender.
	 * @param out     output stream where the message will be sent though.
	 */
	static void sendMessage(String message, String sender, ObjectOutputStream out) {
		try {
			out.writeObject(message);
			out.flush();
			System.out.println(sender + ">" + message);
		} catch (IOException ioException) {
			ioException.printStackTrace();
		}
	}

	/**
	 * Receives a message from the sender. Displays who sent the message.
	 * 
	 * @param in the input stream where the message is coming from.
	 * @return received message.
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	static String receiveMessage(ObjectInputStream in) throws ClassNotFoundException, IOException {
		return (String) in.readObject();
	}
}
