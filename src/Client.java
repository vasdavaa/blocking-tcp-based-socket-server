import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class Client {
	List<String> filesDirectory;
	List<String> serverStorageDirectory;

	ObjectInputStream in;
	ObjectOutputStream out;

	Scanner scan;
	Socket requestSocket;
	String sender = "client";
	String filesDirectoryName = "files";
	String message;

	Client() {
	}

	void run() throws UnknownHostException, IOException {
		try {

			// Create socket
			requestSocket = new Socket("localhost", 8080);
			if (requestSocket.isConnected()) {

				// Input and Output streams
				out = new ObjectOutputStream(requestSocket.getOutputStream());
				in = new ObjectInputStream(requestSocket.getInputStream());

				// Communication
				// Send the word "List" message to get the file list from the server
				CommonUtilities.sendMessage("List", sender, out);
				serverStorageDirectory = new ArrayList<>();

				do {
					try {

						// Waiting for filenames
						message = CommonUtilities.receiveMessage(in);
						System.out.println("server>" + message);

						if (!message.equals("Upload or download? ('u' or 'd')")) {

							// Collect the files to a String array
							serverStorageDirectory.add(message);
						} else {

							// "Upload or download? ('u' or 'd')" message arrived from the server
							System.out.println("Files in the server's storage: " + serverStorageDirectory);
							System.out.println("Files in the local \"files\" directory: "
									+ (filesDirectory = CommonUtilities
											.getFilesFromStorage(filesDirectoryName)));

							// Sending the response ('u' or 'd')
							scan = new Scanner(System.in);
							String operation = userInputFromArray(scan, message + ": ", new String[] { "u", "d" });
							CommonUtilities.sendMessage(operation, sender, out);

							// User input for the file name
							message = "File name: ";
							String fileName;

							if (operation.equals("u")) {
								fileName = userInputFromArray(scan, message,
										filesDirectory.stream().toArray(String[]::new));
							} else {
								fileName = userInputFromArray(scan, message,
										serverStorageDirectory.stream().toArray(String[]::new));
							}

							CommonUtilities.sendMessage(fileName, sender, out);
							System.out.println("filename " + fileName);

							switch (operation) {
							case "u":
								CommonUtilities.sendFile(fileName, filesDirectoryName, out);
								System.out.println("File has been uploaded");
								break;
							case "d":
								CommonUtilities.receiveFile(fileName, filesDirectoryName, in);
								System.out.println("File has been downloaded");
								break;
							}
							message = CommonUtilities.receiveMessage(in);
						}
					} catch (Exception e) {
						System.err.println("data received in unknown format");
					}
				} while (!message.equals("bye"));
			}

		} catch (UnknownHostException unknownHost) {
			System.err.println("You are trying to connect to an unknown host!");
		} catch (IOException ioException) {
			ioException.printStackTrace();
		} finally {

			// Closing the connection
			try {
				scan.close();
				in.close();
				out.close();
				requestSocket.close();
			} catch (IOException ioException) {
				ioException.printStackTrace();
			}
		}
	}

	/**
	 * Creates a regular expression pattern based on the given text(s).
	 * 
	 * @param texts, is the given text(s) which will be used to create the regex.
	 * @return created regex.
	 */
	static String createPattern(String... texts) {
		return Arrays.asList(texts).stream().map(text -> ("^" + text + "$"))
				.collect(Collectors.joining("|"));
	}

	/**
	 * Verified scan from user. It only accepts input which the pattern allows.
	 * 
	 * @param userInput scanner which will be used for the input.
	 * @param message   display the user a simple message about the input.
	 * @param texts     which are the only acceptable options for the user.
	 * @return Verified input from the user.
	 */
	static String userInputFromArray(Scanner userInput, String message, String... texts) {
		Pattern patterWithAcceptedTexts = Pattern.compile(createPattern(texts));
		Matcher matcher;
		String validatedText = null;
		do {
			System.out.print(message);
			matcher = patterWithAcceptedTexts.matcher(validatedText = userInput.nextLine());
		} while (!(matcher.find()));

		return validatedText;
	}

	public static void main(String[] args) {
		Client client = new Client();
		try {
			client.run();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
