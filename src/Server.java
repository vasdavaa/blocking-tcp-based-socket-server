import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
	ObjectInputStream in;
	ObjectOutputStream out;

	String sender = "server";
	String defaultStorageLocation = "store";
	ServerSocket providerSocket;
	Socket connection = null;
	String message;

	Server() {
	}

	void run() {
		try {

			// Creating the server socket
			providerSocket = new ServerSocket(8080);

			// Waiting for the connection
			connection = providerSocket.accept();

			// Input and Output streams
			out = new ObjectOutputStream(connection.getOutputStream());
			in = new ObjectInputStream(connection.getInputStream());

			// Communication
			try {
				do {

					// Waiting for the "List" message
					message = CommonUtilities.receiveMessage(in);
					System.out.println("client>" + message);

					if (message.equals("List")) {

						// Sends the filenames to the client
						CommonUtilities.getFilesFromStorage(defaultStorageLocation).stream()
								.forEach(file -> CommonUtilities.sendMessage(file, sender, out));

						// Wait for the client to select an action
						CommonUtilities.sendMessage("Upload or download? ('u' or 'd')", sender, out);
						message = CommonUtilities.receiveMessage(in);

						// Wait for the client to name the file for the operation
						String fileName = CommonUtilities.receiveMessage(in);

						if (message.equals("u")) {

							// File uploading: it will create a file with the given name in the store
							// directory.
							CommonUtilities.receiveFile(fileName, defaultStorageLocation, in);
						} else if (message.equals("d")) {

							// File downloading: it will send the specific file(which was mentioned by
							// the client) from the store.
							CommonUtilities.sendFile(fileName, defaultStorageLocation, out);
						}
						// Say "bye" to the client, which is a keyword to terminate the client process.
						CommonUtilities.sendMessage("bye", sender, out);

						// Goodbye world
						message = "bye";
					}
				} while (!message.equals("bye"));
			} catch (ClassNotFoundException classnot) {
				System.err.println("Data received in unknown format");
			}

		} catch (IOException ioException) {
			ioException.printStackTrace();
		} finally {

			// Closing the connection
			try {
				in.close();
				out.close();
				providerSocket.close();
			} catch (IOException ioException) {
				ioException.printStackTrace();
			}
		}
	}

	public static void main(String args[]) {
		Server server = new Server();
		server.run();
	}
}