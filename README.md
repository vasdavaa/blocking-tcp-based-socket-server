# Blocking TCP based socket server

1. The client connects to the server and sends a "List" message.
2. The server returns a list of stored files (or a list of previously uploaded files).
3. The client list the files and prompt the user to upload or download? ("u" or "d")
4. In both cases, you must enter the file name along with the extension.
5. The client sends the selected file to the server or downloads the selected file to a specific directory.

## Server viewpoint:

1. After the connection has been established, it reads the files from the store subdirectory and sends the filenames to the client when the "List" message arrives.
2. Waiting for client response('u' or 'd').
3. We get a filename from the client and if 'd' (download) is the operation, we read the file and return its contents.
4. If the operation is 'u' (upload), we will open a new file with the specified name and wait for the data to be written to the file.

## Client viewpoint:

1. After the connection has been established, the client sends a "List" message, waits for a list of returning files, and displays it on the console when it arrives.
2. It will ask the user to select the operation('u'pload or 'd'ownload a file).
3. Then we ask for the file name as well.
4. The client reads the files from the file directory or creates the downloaded file here.
5. If the download action is selected, the client creates the files directory and also creates the file downloaded from the server here.
6. If the upload operation is selected, the client sends the a /files/<filename> file to the server

# Usage: 

1. Start the server program.
2. Start the client program.
3. The client program will list the files that is available at the server for download, and our local "files" directory from which we can upload files to the server. 
Then the client will ask for an operation input. Enter 'u' or 'd' into the console.
4. The client will ask you to enter the file name. Enter the file name in the console. (It does not allow you to upload or download non-existent files)
5. The client downloads or uploads the file to that directory.